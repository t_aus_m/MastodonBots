#!/usr/bin/python3

from mastodon import Mastodon
import urllib.request
import os
import ssl

os.chdir(os.path.dirname(os.path.abspath(__file__)))
mastodon = Mastodon(
    access_token = 'whatthecommit.secret',  # File for "Your access token", can be created unter "Settings" -> "Development" -> "Your applications"
    api_base_url = 'https://botsin.space'   # URL that the bot is hosted on
)

ssl._create_default_https_context = ssl._create_unverified_context
commit = urllib.request.urlopen('http://whatthecommit.com/index.txt').read().decode('utf-8').rstrip()

mastodon.status_post("\"" + commit +"\"")
